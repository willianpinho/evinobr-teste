//
//  NSObject+Extension.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

extension NSObject {
    class func className() -> String {
        let str: String = NSStringFromClass(self)
        let index = str.index(of: ".")
        _ = str.prefix(upTo: index!)
        
        if let index = index {
            return String(str.prefix(upTo: index))
        }
        
        return str
    }
}
