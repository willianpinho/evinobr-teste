//
//  UITableView+Extension.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func registerNibFrom(_ cellClass: UITableViewCell.Type) {
        let nib = UINib(nibName: cellClass.className(), bundle: nil)
        self.register(nib, forCellReuseIdentifier: cellClass.className())
    }
    
    func registerNibs(_ cellClasses : [UITableViewCell.Type]) {
        cellClasses.forEach { self.registerNibFrom($0) }
    }
    
    func dequeueReusableCell<T : UITableViewCell>(ofType type: T.Type) -> T? {
        return self.dequeueReusableCell(withIdentifier: type.className()) as? T
    }
    
    func dequeueReusableCell<T : UITableViewCell>(ofType type: T.Type, forIndexPath indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: type.className(), for: indexPath) as? T
    }
    
}
