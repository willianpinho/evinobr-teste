//
//  Bundle+Extension.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

extension Bundle {
    func loadNib<T : NSObject>(ofType type: T.Type, owner: Any?, options: [AnyHashable : Any]?) -> T? {
        return self.loadNibNamed(T.className(), owner: owner, options: options)?.first as? T
    }
}
