//
//  ProfilePresenter.swift
//  evinobr
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

protocol ProfilePresenterView: NSObjectProtocol {
    func showAlert(title: String, message: String)
}

class ProfilePresenter: NSObject {
    
    var view : ProfilePresenterView?
    
    func setViewDelegate(view: ProfilePresenterView) {
        self.view = view
    }
    
}
