//
//  ProfileViewController.swift
//  evinobr
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import MessageUI
import Firebase
import FBSDKLoginKit
import PKHUD


class ProfileViewController: UIViewController {
    var presenter = ProfilePresenter()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    let options = ["Envie uma mensagem": "mensagem", "Siga no Facebook": "fb", "Siga no Instagram": "insta"]
    var cells = [UITableViewCell]()

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.configureTableView(tableView: self.tableView)
        self.loadOptions()
        self.customLayout()
        
    }
    
    func configureTableView(tableView: UITableView) {
        self.tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func loadOptions() {
        for (option,icon) in options {
            self.updateCells(option: option, icon: icon)
        }
        
        self.setup(tableView: self.tableView)
    }
    
    func updateCells(option: String, icon: String) {
        if let ptvc = self.tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
            ptvc.option.text = option
            ptvc.imageView?.image = UIImage(named: icon)
            self.cells.append(ptvc)
        }
        
    }
    
    func customLayout() {
    
        self.loginButton.addTarget(self, action: #selector(loginWithFacebook), for: UIControlEvents.touchUpInside)
        self.logoutButton.addTarget(self, action: #selector(logoutUser), for: UIControlEvents.touchUpInside)
        
        self.profilePhoto.layer.cornerRadius = self.profilePhoto.frame.size.width / 2
        self.profilePhoto.clipsToBounds = true
        
        self.updateButtons()
        
        self.title = "Configurações"
        
    }
    
    @objc func logoutUser() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.updateButtons()
        } catch let signOutError as NSError {
            showAlert(title: "Erro", message: "Não conseguimos sair do app")
            print(signOutError)
        }
        
    }
    
    @objc func loginWithFacebook() {
        HUD.show(.progress)
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            if (result?.isCancelled)! {
                print("Facebook Login Cancelled")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            
            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    self.showAlert(title: "Erro", message: "Erro ao conectar com o facebook")
                    print(error)
                } else {
                    self.updateButtons()
                    PKHUD.sharedHUD.hide()
                }
            }
        }
    }
    
    func updateButtons(){
        if let user = Auth.auth().currentUser {
            self.profilePhoto.kf.setImage(with: user.photoURL)
            self.profilePhoto.reloadInputViews()
            self.profileName.text = user.displayName
            
            self.loginButton.isHidden = true
            self.profilePhoto.isHidden = false
            self.profileName.isHidden = false
            self.logoutButton.isHidden = false
        } else {
            self.loginButton.isHidden = false
            self.profilePhoto.isHidden = true
            self.profileName.isHidden = true
            self.logoutButton.isHidden = true
        }
    }
    
    @objc func backToCategories() {
        if ((self.navigationController) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func sendMessage() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as MFMailComposeViewControllerDelegate
            mail.setToRecipients(["willjrpp@gmail.com"])
            mail.setSubject("Oi Willian!")
            mail.setMessageBody("<p>Pode abrir o coração</p>", isHTML: true)
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
            self.showAlert(title: "Ops", message: "Parece que você não tem Mail App")
        }
    }
    
    func followFacebook() {
        self.open(scheme: "fb://profile/100000116305065", url: "https://www.facebook.com/100000116305065")
    }
    
    func followInstagram() {
        self.open(scheme: "instagram://user?username=will.pinho", url: "https://www.instagram.com/will.pinho")
    }
    
    
    func open(scheme: String, url: String) {
        if let scheme = URL(string: scheme) {
            UIApplication.shared.open(scheme, options: [:], completionHandler: {
                (success) in
                if success {
                    
                } else {
                    UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
                }
            })
        }
    }
    
}

extension ProfileViewController: ProfilePresenterView {
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func setup(tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.sendMessage()
        case 1:
            self.followFacebook()
        case 2:
            self.followInstagram()
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
}

extension ProfileViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
