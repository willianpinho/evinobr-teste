//
//  HerbPresenter.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

protocol HerbPresenterView: NSObjectProtocol {
    func showAlert(title: String, message: String)
    func updateCells(herb: Herb)
}

class HerbPresenter: NSObject {
    var view : HerbPresenterView?
    
    func setViewDelegate(view: HerbPresenterView) {
        self.view = view
    }
    
    func loadHerb(herb: Herb, completion: @escaping (Bool?, Herb?, String?) -> Void) {
        HerbService.getHerbFromServer(herb: herb) { (success, herb) in
            if success! {
                completion(true, herb, nil)
            } else {
                completion(false, nil, "Erro ao Carregar Erva Escolhida")
            }
        }
    }
}
