//
//  HerbViewController.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import PKHUD

class HerbViewController: UIViewController {
    var presenter = HerbPresenter()
    var herb: Herb!
    
    @IBOutlet weak var tableView: UITableView!
    
    var cells = [UITableViewCell]()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        HUD.show(.progress)
        self.configureTableView(tableView: self.tableView)
        self.loadHerb()
        self.customLayout()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customLayout() {
        self.title = self.herb.name?.capitalized
        
    }
    
    func loadHerb() {
        presenter.loadHerb(herb: self.herb) { (success, herb, message) in
            if success! {
                self.updateCells(herb: herb!)
                self.setup(tableView: self.tableView)
                PKHUD.sharedHUD.hide()
            } else {
                self.showAlert(title: "Ops", message: message!)
                PKHUD.sharedHUD.hide()
            }
        }
    }
    
    func configureTableView(tableView: UITableView) {
        self.tableView.register(UINib(nibName: "HerbImageTableViewCell", bundle: nil), forCellReuseIdentifier: "HerbImageTableViewCell")
        self.tableView.register(UINib(nibName: "HerbHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HerbHeaderTableViewCell")
        self.tableView.register(UINib(nibName: "HerbDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "HerbDescriptionTableViewCell")
        
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
}

extension HerbViewController: HerbPresenterView {
    func updateCells(herb: Herb) {
        
        if let hitvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbImageTableViewCell") as? HerbImageTableViewCell {
            if let urlPhoto = herb.urlImage {
                let url = URL(string: urlPhoto)
                hitvc.bgImageView.kf.setImage(with: url)
                hitvc.bgImageView.reloadInputViews()
                cells.append(hitvc)
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let description = herb.description {
                hhtvc.header.text = ""
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = description
                    cells.append(hdtvc)
                }
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let name = herb.scientificName {
                hhtvc.header.text = "Nome Científico"
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = name
                    cells.append(hdtvc)
                }
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let name = herb.popularName {
                hhtvc.header.text = "Nomes Populares"
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = name
                    cells.append(hdtvc)
                }
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let purpose = herb.medicinalPurposes {
                hhtvc.header.text = "Fins Medicinais"
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = purpose
                    cells.append(hdtvc)
                }
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let howToUse = herb.howToUse {
                hhtvc.header.text = "Como Usar"
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = howToUse
                    cells.append(hdtvc)
                }
            }
        }
        
        if let hhtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbHeaderTableViewCell") as? HerbHeaderTableViewCell {
            
            if let comments = herb.comments {
                hhtvc.header.text = "Atenção"
                cells.append(hhtvc)
                
                if let hdtvc = self.tableView.dequeueReusableCell(withIdentifier: "HerbDescriptionTableViewCell") as? HerbDescriptionTableViewCell {
                    hdtvc.descriptionTextView.text = comments
                    cells.append(hdtvc)
                }
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
}

extension HerbViewController: UITableViewDataSource, UITableViewDelegate {
    func setup(tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return self.cells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
