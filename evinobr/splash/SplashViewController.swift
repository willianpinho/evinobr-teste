//
//  SplashViewController.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase

class SplashViewController: UIViewController {

    var presenter = SplashPresenter()

    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var enterFacebookButton: UIButton!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.customLayout()
    }

    func customLayout() {
        self.enterButton.addTarget(self, action: #selector(goToMainApp), for: UIControlEvents.touchUpInside)
        self.enterFacebookButton.addTarget(self, action: #selector(loginWithFacebook), for: UIControlEvents.touchUpInside)
    }
    
    @objc func goToMainApp() {
        let storyboard = UIStoryboard(name: "Herbs", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.present(vc!, animated: false, completion: nil)
    }
    
    @objc func loginWithFacebook() {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in

            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }

            if (result?.isCancelled)! {
                print("Facebook Login Cancelled")
                return
            }

            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)

            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    self.showAlert(title: "Erro", message: "Erro ao conectar com o facebook")
                    print(error)
                } else {
                    self.goToMainApp()
                }
            }
        }
    }

}

extension SplashViewController: SplashPresenterView {
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
}
