//
//  SplashPresenter.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

protocol SplashPresenterView: NSObjectProtocol {
    func showAlert(title: String, message: String)

}

class SplashPresenter: NSObject {
    var view : SplashPresenterView?
    
    func setViewDelegate(view: SplashPresenterView) {
        self.view = view
    }
}
