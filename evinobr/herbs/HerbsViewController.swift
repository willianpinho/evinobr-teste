//
//  HerbsViewController.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import PKHUD
import Kingfisher
import Firebase


class HerbsViewController: UIViewController {
    @IBOutlet weak var herbsCollectionView: UICollectionView!
    
    var presenter = HerbsPresenter()
    var herbs: [Herb] = []
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress)
        self.setPresenterDelegate()
        self.customLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadHerbs()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customLayout() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        
        let rightBarButton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cogwheel"), style: .plain, target: self, action: #selector(goToSettings))
        
        
        
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.title = "Plantas Medicinais"
    }
    
    func loadHerbs() {
        presenter.loadHerbsFromServer { (success, herbs, message) in
            if success! {
                self.herbs = herbs!
                self.setup(collectionView: self.herbsCollectionView)
                PKHUD.sharedHUD.hide()
            } else {
                self.showAlert(title: "Ops", message: message!)
                PKHUD.sharedHUD.hide()
            }
        }
    }
    
    @objc func goToSettings() {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func goToHerb(herb: Herb) {
        let storyboard = UIStoryboard(name: "Herb", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! HerbViewController
        vc.herb = herb
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

extension HerbsViewController: HerbsPresenterView {
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
}

extension HerbsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func setup(collectionView: UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = self.view.frame.size.width/2
        return CGSize(width: size, height: size)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset: CGFloat = 0
        
        let edgeInsets = UIEdgeInsets.init(top: inset, left: inset, bottom: inset, right: inset)
        return edgeInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.herbs.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HerbsCollectionViewCell", for: indexPath) as! HerbsCollectionViewCell
        
        let herb = self.herbs[indexPath.row]
        cell.name.text = herb.name?.capitalized
        
        let urlPhoto = herb.urlImage
        let url = URL(string: urlPhoto!)
        cell.image.kf.setImage(with: url)
        cell.image.reloadInputViews()
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.goToHerb(herb: self.herbs[indexPath.row])
    }
}
