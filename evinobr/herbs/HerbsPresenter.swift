//
//  HerbsPresenter.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

protocol HerbsPresenterView: NSObjectProtocol {
    func showAlert(title: String, message: String)
}

class HerbsPresenter: NSObject {
    var view : HerbsPresenterView?
    
    func setViewDelegate(view: HerbsPresenterView) {
        self.view = view
    }
    
    func loadHerbsFromServer(completion: @escaping (Bool?, [Herb]?, String?) -> Void) {
        HerbService.getHerbsFromServer { (success, herbs) in
            if success! {
                completion(true, herbs, nil)
            } else {
                completion(false, nil, "Erro ao Carregar Ervas")
            }
        }
    }
}
