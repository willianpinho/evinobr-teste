//
//  HerbsCollectionViewCell.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit

class HerbsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
