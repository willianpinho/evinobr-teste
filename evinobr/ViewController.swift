//
//  ViewController.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if (Auth.auth().currentUser != nil) {
            self.perform(#selector(goToMainApp), with: nil, afterDelay: 1.0)
        } else {
            self.perform(#selector(goToLoginViewController), with: nil, afterDelay: 1.0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func goToLoginViewController() {
        let storyboard = UIStoryboard(name: "Splash", bundle: nil)
        let lvc = storyboard.instantiateInitialViewController()
        
        lvc?.modalTransitionStyle = .crossDissolve
        self.present(lvc!, animated: true, completion: nil)
    }
    
    @objc func goToMainApp() {
        let storyboard = UIStoryboard(name: "Herbs", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        vc?.modalTransitionStyle = .crossDissolve
        self.present(vc!, animated: true, completion: nil)
    }


}

