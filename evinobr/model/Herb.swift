//
//  Herb.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Herb {
    let key: String?
    let comments: String?
    let description: String?
    let howToUse: String?
    let medicinalPurposes: String?
    let name: String?
    let popularName: String?
    let scientificName: String?
    let urlImage: String?
    let ref: DatabaseReference?
    
    init(key: String = "", comments: String, description: String, howToUse: String, medicinalPurposes: String, name: String, popularName: String, scientificName: String, urlImage: String) {
        self.key = key
        self.comments = comments
        self.description = description
        self.howToUse = howToUse
        self.medicinalPurposes = medicinalPurposes
        self.name = name
        self.popularName = popularName
        self.scientificName = scientificName
        self.urlImage = urlImage
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        comments = snapshotValue["comments"] as? String
        description = snapshotValue["description"] as? String
        howToUse = snapshotValue["how_to_use"] as? String
        medicinalPurposes = snapshotValue["medicinal_purposes"] as? String
        name = snapshotValue["name"] as? String
        popularName = snapshotValue["popular_name"] as? String
        scientificName = snapshotValue["scientific_name"] as? String
        urlImage = snapshotValue["url_image"] as? String
        
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name

        ]
    }

}
