//
//  HerbService.swift
//  evinobr-teste
//
//  Created by Willian Pinho on 08/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class HerbService {
    static func getHerbsFromServer(completion: @escaping (Bool?, _ herbs: [Herb]?) -> Void) {
        let ref = Database.database().reference(withPath: "herbs")
        ref.queryOrdered(byChild: "name").observe(.value, with: { snapshot in
            var herbs: [Herb] = []
            for h in snapshot.children {
                let herb = Herb(snapshot: h as! DataSnapshot)
                herbs.append(herb)
            }

            if herbs.isEmpty {
                completion(false, nil)
            } else {
                completion(true, herbs)
            }
        })        
    }
    
    static func getHerbFromServer(herb: Herb, completion: @escaping (Bool?, _ herb: Herb?) -> Void) {
        let currentHerb = herb.key
        let ref = Database.database().reference(withPath: "herbs")
        ref.child(currentHerb!).observeSingleEvent(of: .value) { (snapshot) in
            if !snapshot.exists() { completion(false, nil) }
            let herb = Herb(snapshot: snapshot)
            completion(true, herb)
        }
    }
}

